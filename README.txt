The Batch Assist module helps developers create and manage custom batch
processes. Includes hooks to create new batch processes, retrieve data, and
process functions.

Installation
------------
Standard module installation applies.

Menus
-----
The only menu items are for the settings page and assisted batches.

Settings
--------
The settings page is at Administer >> Config >> System >> Batch Assist.

This is where you administer the assisted batches.

Permissions
-----------
The settings page is controlled by the "administer batch_assist" permission.
