<?php

/**
 * @file
 * Hooks provided by the Batch Assist module.
 */

/**
 * @defgroup batch_assist_api_hooks Redirect API Hooks
 * @{
 * @TODO define hooks
 * @}
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Append a new assisted batch.
 *
 * This hook is invoked from _batch_assist_info().
 *
 * @array $info
 *   The assisted batch processes.
 *
 * @see _batch_assist_info()
 * @ingroup batch_assist_api_hooks
 */
function hook_batch_assist_info($info = array()) {
  // example
  $info['YOUR_MACHINE_NAME'] = array(
    'title' => t('Batch Title'),
    'description' => t('Batch description.'),
    'query_limit' => array(
      '#default_value' => 5,
      '#disabled' => TRUE,
    ),
    'custom_fields' => array(
      'YOUR_CUSTOM_FIELD' => array(
        '#type' => 'textfield',
        '#title' => t('Custom field title'),
        '#description' => t('Custom field description.'),
      ),
    ),
    'disable_hooks' => array(
      'HOOK_NAME' => array(
        'MODULE_NAME',
      ),
    ),
  );

  return $info;
}

/**
 * Retrieve the query for the next batch.
 *
 * This hook is invoked from both _batch_assist_retrieve().
 *
 * @array $values
 *   The batch form values.
 *
 * @array $context
 *   The batch variable storage.
 *
 * @see _batch_assist_retrieve()
 * @ingroup batch_assist_api_hooks
 */
function hook_batch_assist_retrieve($values, $context) {
  // example
  $query = db_select('users', 'u');
  $query->addField('u', 'uid', 'current_id');
  $query->where('u.uid > :last_id', array(':last_id' => $context['sandbox']['last_id']))
    ->orderBy('u.uid', 'ASC');

  return $query;
}

/**
 * Process the current batch.
 *
 * This hook is invoked from _batch_assist_process().
 *
 * @array $values
 *   The batch form values.
 *
 * @array $context
 *   The batch variable storage.
 *
 * @see _batch_assist_process()
 * @ingroup batch_assist_api_hooks
 */
function hook_batch_assist_process($values, $context) {
  // example
  $nid = $context['sandbox']['current_id'];

  $node = node_load($nid);
  // process function here...
}

/**
 * Callback for the current batch (Optional).
 *
 * This hook is invoked from _batch_assist_callback().
 *
 * @array $values
 *   The batch form values.
 *
 * @array $context
 *   The batch variable storage.
 *
 * @array $error
 *   The error string, if any.
 *
 * @see _batch_assist_callback()
 * @ingroup batch_assist_api_hooks
 */
function hook_batch_assist_callback($values, $context, $error) {
  // example
  if ($context['finished']) {
    drupal_set_message('Operation finished!');
  }
  else {
    drupal_set_message('Single batch process complete. Still processing...');
  }
}

/**
 * @} End of "addtogroup hooks".
 */
