<?php
/**
 * @file
 * Menu callbacks, form callbacks and helpers.
 */

/**
 * Menu callback function to display an overview of available assisted batches.
 */
function batch_assist_overview_page() {
  $header = array(
    t('Name'),
  );
  $rows = array();

  if ($assisted_batches = batch_assist_get_all_assisted_batches()) {
    foreach ($assisted_batches as $machine_name => $assisted_batch) {
      // Skip if user does not have access.
      if (!user_access('administer batch_assist ' . $machine_name . ' instance')) {
        continue;
      }

      $rows[] = array(
        theme('assisted_batch_admin_overview', array(
          'machine_name' => $machine_name,
          'title' => $assisted_batch['title'],
        )),
      );
    }
  }

  if (!$rows) {
    $rows[] = array(
      theme('assisted_batch_admin_overview', array(
        'machine_name' => '',
        'title' => t('There are no batch_assist instances or you do not have access to any.'),
      )),
    );
  }

  return theme('table', array('header' => $header, 'rows' => $rows));
}

/**
 * Menu callback function to allow batch run.
 */
function batch_assist_form($form, &$form_state, $machine_name) {
  $assisted_batch = batch_assist_get_assisted_batch($machine_name);
  $custom_fields = isset($assisted_batch['custom_fields']) ? $assisted_batch['custom_fields'] : NULL;
  $default_values = array(
    'machine_name' => $machine_name,
    'operation_count' => 1,
  );

  if ($custom_fields) {
    foreach ($custom_fields as $custom_field_name => $custom_field_settings) {
      if (isset($custom_field_settings['#default_value'])) {
        $default_values[$custom_field_name] = $custom_field_settings['#default_value'];
      }
    }
  }

  $default_context = array(
    'sandbox' => array('last_id' => 0),
  );
  $maximum_result = _batch_assist_retrieve($default_values, $default_context);
  $maximum = $maximum_result ? $maximum_result->rowCount() : 0;

  $form['description'] = array(
    '#type' => 'markup',
    '#markup' => $assisted_batch['description'],
  );
  $form['machine_name'] = array(
    '#type' => 'hidden',
    '#default_value' => $machine_name,
  );
  // @TODO leverage operation_count to display progress
  $form['operation_count'] = array(
    '#type' => 'hidden',
    '#default_value' => $default_values['operation_count'],
  );
  $form['maximum'] = array(
    '#type' => 'hidden',
    '#default_value' => $maximum,
  );
  $form['batch_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Batch limit'),
    '#description' => t('Set the batch limit. Maximum: @max', array('@max' => $maximum)),
    '#default_value' => $maximum,
    '#required' => TRUE,
  );
  $form['query_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Query limit'),
    '#description' => t('Set the query limit.'),
    '#default_value' => isset($assisted_batch['query_limit']) && isset($assisted_batch['query_limit']['#default_value']) ? $assisted_batch['query_limit']['#default_value'] : 50,
    '#required' => TRUE,
    '#disabled' => isset($assisted_batch['query_limit']) && isset($assisted_batch['query_limit']['#disabled']) ? $assisted_batch['query_limit']['#disabled'] : FALSE,
  );
  $form['redirect'] = array(
    '#type' => 'textfield',
    '#title' => t('Redirect'),
    '#description' => t('Optionally redirect upon completion.'),
  );

  if ($custom_fields) {
    $form['custom_fields'] = array(
      '#type' => 'fieldset',
      '#title' => t('Custom Fields'),
    );

    foreach ($custom_fields as $custom_field_name => $custom_field_settings) {
      $form['custom_fields'][$custom_field_name] = $custom_field_settings;
    }
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );
  $form['#validate'][] = 'batch_assist_form_validate';

  // If no items, prevent submission.
  if ($maximum <= 0) {
    drupal_set_message(t('There are no items to process. As a result, this form is disabled.'));

    $form['#disabled'] = TRUE;

    // Recursively disable form elements that are specifically enabled.
    foreach ($form as &$form_element) {
      if (
        isset($form_element['#disabled']) &&
        !$form_element['#disabled']
      ) {
        $form_element['#disabled'] = TRUE;
      }
      else if (
        isset($form_element['#type']) &&
        $form_element['#type'] == 'fieldset'
      ) {
        foreach ($form_element as &$fieldset_element) {
          if (
            isset($fieldset_element['#disabled']) &&
            !$fieldset_element['#disabled']
          ) {
            $fieldset_element['#disabled'] = TRUE;
          }
        }
      }
    }
  }

  return $form;
}

/**
 * Validation function for batch form.
 */
function batch_assist_form_validate($form, &$form_state) {
  // Set lock to prevent multiple submits.
  if (!lock_acquire('batch_assist_form_lock')) {
    form_set_error('', t('Form already submitted.'));
  }

  $values = array('operation_count', 'batch_limit', 'query_limit');

  foreach ($values as $name) {
    $value = $form_state['values'][$name];

    if (
      !$value ||
      !is_numeric($value) ||
      $value <= 0
    ) {
      form_set_error($name, t('Field @name is invalid.', array('@name' => $name)));
    }

    if (
      $name == 'batch_limit' &&
      $value > $form_state['values']['maximum']
    ) {
      form_set_error($name, t('Field @name is above the maximum allowed.', array('@name' => $name)));
    }
  }

  // Release lock.
  lock_release('batch_assist_form_lock');
}

/**
 * Submit function for batch form.
 */
function batch_assist_form_submit(&$form, &$form_state) {
  // Reset counter for debug information.
  $_SESSION['http_request_count'] = 0;

  $redirect = $form_state['values']['redirect'];
  // Isolate form values.
  $ignore = array('redirect', 'submit', 'form_build_id', 'form_token', 'form_id','op');
  $values = array_diff_key($form_state['values'], array_flip($ignore));

  // Restrict query limit to batch limit
  $values['query_limit'] = $values['query_limit'] > $values['batch_limit'] ? $values['batch_limit'] : $values['query_limit'];

  // Execute the function.
  $batch = batch_assist_batch($values);
  batch_set($batch);

  // Redirect upon completion.
  if ($redirect) {
    batch_process($redirect);
  }
}
