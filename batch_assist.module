<?php
/**
 * @file
 * Assist the creation and management of batch processes.
 */

define('CONFIGURE_URL', 'admin/config/system/batch-assist');
define('DEBUG', FALSE);

/**
 * Implements hook_permission().
 */
function batch_assist_permission() {
  $permissions['administer batch_assist'] = array(
    'title' => t('Administer Batch Assist module'),
    'description' => t('Perform administration tasks for Batch Assist module.'),
  );

  if ($assisted_batches = batch_assist_get_all_assisted_batches()) {
    foreach ($assisted_batches as $machine_name => $assisted_batch) {
      $permissions['administer batch_assist ' . $machine_name . ' instance'] = array(
        'title' => t('Administer ' . $assisted_batch['title'] . ' Batch Assist instance'),
        'description' => t('Perform administration tasks for ' . $assisted_batch['title'] . ' Batch Assist instance.'),
      );
    }
  }

  return $permissions;
}

/**
 * Implements hook_help().
 */
function batch_assist_help($path, $arg) {
  switch ($path) {
    case CONFIGURE_URL:
      return t('Assist the creation and management of batch processes.');
  }
}

/**
 * Implements hook_menu().
 */
function batch_assist_menu() {
  $items[CONFIGURE_URL] = array(
    'title' => t('Batch Assist'),
    'description' => t('Overview for Batch Assist'),
    'page callback' => 'batch_assist_overview_page',
    'access arguments' => array('administer batch_assist'),
    'file' => 'batch_assist.admin.inc',
    'type' => MENU_NORMAL_ITEM,
    'weight' => 0,
  );

  if ($assisted_batches = batch_assist_get_all_assisted_batches()) {
    foreach ($assisted_batches as $machine_name => $assisted_batch) {
      $items[CONFIGURE_URL . '/' . $machine_name] = array(
        'title' => $assisted_batch['title'],
        'description' => $assisted_batch['description'],
        'page callback' => 'drupal_get_form',
        'page arguments' => array('batch_assist_form', $machine_name),
        'access arguments' => array('administer batch_assist ' . $machine_name . ' instance'),
        'file' => 'batch_assist.admin.inc',
      );
    }
  }

  return $items;
}

/**
 * Implements hook_theme().
 */
function batch_assist_theme() {
  return array(
    'assisted_batch_admin_overview' => array(
      'variables' => array(
        'machine_name' => NULL,
        'title' => NULL,
      ),
    ),
  );
}

/**
 * Builds an overview of an assisted batch for display to an administrator.
 *
 * @param array $variables
 *   An array of variables used to generate the display.
 *
 * @ingroup themeable
 */
function theme_assisted_batch_admin_overview(array $variables) {
  $machine_name = $variables['machine_name'];

  if (!$machine_name) {
    $output = t('@title', array('@title' => $variables['title']));
  }
  else {
    $output = t('<a href="@url">@title</a>', array('@url' => url() . CONFIGURE_URL . '/' . $machine_name, '@title' => $variables['title']));
    $output .= ' <small>' . t('(Machine name: @machine_name)', array('@machine_name' => $machine_name)) . '</small>';
  }

  return $output;
}

/**
 * Implements hook_hook_info().
 */
function batch_assist_hook_info() {
  $hooks = array(
    'batch_assist_info',
  );

  return array_fill_keys($hooks, array('group' => 'batch_assist'));
}

/**
 * Helper function to retrieve assisted batches info.
 */
function _batch_assist_info() {
  // @TODO asses drupal_static
  $info = &drupal_static(__FUNCTION__);

  if (isset($info)) {
    return $info;
  }

  $cache_info = array(
    'key' => 'batch_assist:info',
    'enabled' => !DEBUG,
  );
  $cache = $cache_info['enabled'] ? cache_get($cache_info['key']) : FALSE;

  if (
    $cache &&
    isset($cache->data)
  ) {
    $info = $cache->data;
  }
  else {
    $info = module_invoke_all('batch_assist_info');
    drupal_alter('batch_assist_info', $info);
    $info = _batch_assist_info_validate($info);

    if ($cache_info['enabled']) {
      cache_set($cache_info['key'], $info);
    }
  }

  return $info;
}

/**
 * Helper function to validate assisted batches info.
 */
function _batch_assist_info_validate($info) {
  if (!$info) {
    return $info;
  }

  foreach($info as $machine_name => $assisted_batch) {
    $error = FALSE;

    // Confirm that retrieve and process hooks exist.
    if (
      !function_exists($machine_name . '_batch_assist_retrieve') ||
      !function_exists($machine_name . '_batch_assist_process')
    ) {
      $error = TRUE;
      watchdog('batch_assist', 'Assisted batch @machine_name is invalid.', array('@machine_name' => $machine_name));
    }

    $required = array('title', 'description');

    foreach ($required as $key) {
      if (!isset($assisted_batch[$key])) {
        $error = TRUE;
        watchdog('batch_assist', 'Required key not set: @key', array('@key' => $key));
      }
    }

    if ($error) {
      unset($info[$machine_name]);
    }
  }

  return $info;
}

/**
 * Helper function to retrieve a specific assisted batch.
 */
function batch_assist_get_assisted_batch($machine_name) {
  $assisted_batches = batch_assist_get_all_assisted_batches();

  return $assisted_batches && isset($assisted_batches[$machine_name]) ? $assisted_batches[$machine_name] : NULL;
}

/**
 * Helper function to retrieve all assisted batches.
 */
function batch_assist_get_all_assisted_batches() {
  return _batch_assist_info();
}

/**
 * Helper function to retrieve a specific assisted batch.
 */
function _batch_assist_retrieve($values, $context) {
  $function = $values['machine_name'] . '_batch_assist_retrieve';

  if (function_exists($function)) {
    $query = call_user_func_array($function, array($values, $context));

    // Check for NULL query.
    if (!$query) {
      return NULL;
    }

    if (isset($values['query_limit'])) {
      $query = $query->extend('PagerDefault')
        ->limit($values['query_limit']);
    }

    return $query->execute();
  }

  return NULL;
}

/**
 * Helper function to process a specific assisted batch.
 */
function _batch_assist_process($values, $context) {
  $function = $values['machine_name'] . '_batch_assist_process';

  if (function_exists($function)) {
    variable_set('batch_assist_active_batch', $values['machine_name']);

    return call_user_func_array($function, array($values, $context));
  }

  return NULL;
}

/**
 * Callback function for a specific assisted batch (Optional).
 */
function _batch_assist_callback($values, $context, $error) {
  $function = $values['machine_name'] . '_batch_assist_callback';

  if (function_exists($function)) {
    return call_user_func_array($function, array($values, $context, $error));
  }

  return NULL;
}

/**
 * Implements hook_batch().
 */
function batch_assist_batch($values) {
  drupal_set_message(
    t('There are @batch_limit items for assisted batch @machine_name, so @operation_count will require @iterations HTTP requests.',
      array(
        '@machine_name' => $values['machine_name'],
        '@operation_count' => format_plural($values['operation_count'], 'it', 'each of its @count operations'),
        '@batch_limit' => $values['batch_limit'],
        '@iterations' => ceil($values['batch_limit'] / $values['query_limit']),
      )
    )
  );

  $operations = array();
  for ($i = 0; $i < $values['operation_count']; $i++) {
    $operations[] = array(
      'batch_assist_operation',
      array(
        $values,
        t('(Operation @operation)', array('@operation' => $i))
      ),
    );
  }

  $batch = array(
    'operations' => $operations,
    'finished' => 'batch_assist_finished',
    'title' => t('Processing batch...'),
    'init_message' => t('Batch is starting...'),
    'progress_message' => t('Processed @current out of @total (@elapsed).'),
    'error_message' => t('Batch has encountered an error.'),
  );

  return $batch;
}

/**
 * Batch operation : load all items in limited groups.
 *
 * After each limited group control is returned to the batch API for later
 * continuation.
 */
function batch_assist_operation($values, $operation_details, &$context) {
  // Use the $context['sandbox'] at your convenience to store the
  // information needed to track progression between successive calls.
  if (empty($context['sandbox'])) {
    $context['sandbox'] = array(
      'progress' => 0,
      'current_id' => 0,
      'batch_limit' => $values['batch_limit']
    );
  }

  $context['sandbox']['last_id'] = $context['sandbox']['current_id'];
  $batch_limit = $context['sandbox']['batch_limit'];
  $error = '';

  // Retrieve the next group of items.
  $result = _batch_assist_retrieve($values, $context);

  if (
    $result &&
    $result->rowCount()
  ) {
    foreach ($result as $row) {
      if (isset($row->current_id)) {
        foreach ($row as $key => $value) {
          $context['sandbox'][$key] = $value;
        }

        // Update our progress information.
        $context['sandbox']['progress']++;
        $context['message'] = 'Current ID: ' . $context['sandbox']['current_id'] . '. Progress: ' . $context['sandbox']['progress'] . ' / ' . $batch_limit;

        // Store some results for post-processing in the 'finished' callback.
        $context['results'][] = $context['sandbox']['current_id'] . ' : ' . $operation_details;

        // Process current batch.
        _batch_assist_process($values, $context);

        if (
          $context['sandbox']['current_id'] != 0 &&
          $context['sandbox']['last_id'] != 0 &&
          $context['sandbox']['last_id'] == $context['sandbox']['current_id']
        ) {
          $error = t('Current ID same as last ID.');
        }
      }
      else {
        $error = t('Result must return ID field.');
      }
    }
  }
  else {
    $error = t('Result is empty.');
  }

  if ($error) {
    drupal_set_message($error, 'error');

    $context['finished'] = 1;
  }
  else {
    // Inform the batch engine that we are not finished,
    // and provide an estimation of the completion level we reached.
    if ($context['sandbox']['progress'] != $batch_limit) {
      $context['finished'] = ($context['sandbox']['progress'] >= $batch_limit);
    }
  }

  if (DEBUG) {
    watchdog('batch_assist', 'context: ' . $context['sandbox']['progress'] . ', batch_limit: ' . $batch_limit);
  }

  _batch_assist_update_http_requests();
  _batch_assist_callback($values, $context, $error);
}

/**
 * Callback function for batch process.
 */
function batch_assist_finished($success, $results, $operations) {
  if ($success) {
    $message = t('@count results processed in @requests HTTP requests. @time',
      array(
        '@count' => count($results),
        '@requests' => _batch_assist_get_http_requests(),
        '@time' => date('g:ia')
      )
    );
    $message_type = 'status';
  }
  else {
    // An error occurred.
    // $operations contains the operations that remained unprocessed.
    $error_operation = reset($operations);
    $message = t('An error occurred while processing @operation with arguments : @args',
      array(
        '@operation' => $error_operation[0],
        '@args' => print_r($error_operation[0], TRUE),
      )
    );
    $message_type = 'error';
  }

  if ($message) {
    drupal_set_message($message, $message_type);
  }

  variable_set('batch_assist_active_batch', '');
}

/**
 * Helper function to increment HTTP requests in a session variable.
 */
function _batch_assist_update_http_requests() {
  $_SESSION['http_request_count']++;
}

/**
 * Helper function to count the HTTP requests in a session variable.
 *
 * @return int
 *   Number of requests.
 */
function _batch_assist_get_http_requests() {
  return !empty($_SESSION['http_request_count']) ? $_SESSION['http_request_count'] : 0;
}

/**
 * Implements hook_module_implements_alter().
 */
function batch_assist_module_implements_alter(&$implementation, $hook) {
  // Optionally disable hook implementations per active batch.
  if ($active_batch = variable_get('batch_assist_active_batch', '')) {
    $assisted_batch = batch_assist_get_assisted_batch($active_batch);
    $disable_hooks = isset($assisted_batch['disable_hooks']) ? $assisted_batch['disable_hooks'] : array();

    if (
      $disable_hooks &&
      is_array($disable_hooks) &&
      isset($disable_hooks[$hook])
    ) {
      foreach ($disable_hooks[$hook] as $module) {
        // Wildcard remove all hook implementations.
        if ($module == '*') {
          $implementation = array();

          break;
        }
        else if (isset($implementation[$module])) {
          unset($implementation[$module]);
        }
      }
    }
  }
}
